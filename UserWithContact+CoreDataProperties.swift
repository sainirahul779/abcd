//
//  UserWithContact+CoreDataProperties.swift
//  abcd
//
//  Created by Rahul Saini on 29/05/18.
//  Copyright © 2018 Rahul Saini. All rights reserved.
//
//

import Foundation
import CoreData


extension UserWithContact {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<UserWithContact> {
        return NSFetchRequest<UserWithContact>(entityName: "UserWithContact")
    }

    @NSManaged public var firtName: String?
    @NSManaged public var lastName: String?
    @NSManaged public var contacts: NSSet?

}

// MARK: Generated accessors for contacts
extension UserWithContact {

    @objc(addContactsObject:)
    @NSManaged public func addToContacts(_ value: Contact)

    @objc(removeContactsObject:)
    @NSManaged public func removeFromContacts(_ value: Contact)

    @objc(addContacts:)
    @NSManaged public func addToContacts(_ values: NSSet)

    @objc(removeContacts:)
    @NSManaged public func removeFromContacts(_ values: NSSet)

}
