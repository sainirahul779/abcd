//
//  NSFetchResultViewController.swift
//  abcd
//
//  Created by Rahul Saini on 24/05/18.
//  Copyright © 2018 Rahul Saini. All rights reserved.
//

import UIKit
import CoreData

class NSFetchResultViewController: UIViewController {

  @IBAction func addButton(_ sender: Any) {
    let alert = UIAlertController(title: "Add Pepole", message: "", preferredStyle: .alert)
    let addAction = UIAlertAction(title: "Save", style: .default, handler: {action in
      let name = alert.textFields?.first?.text ?? ""
      let desi = alert.textFields![1].text ?? ""
      self.addPepole(withName: name, withDesi: desi)
      
    })
    let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
    
    alert.addTextField()
    alert.addTextField()
    alert.addAction(addAction)
    alert.addAction(cancelAction)
    self.present(alert, animated: true, completion: nil)
  }
  
 
  
  @IBOutlet weak var tableView: UITableView!
  var fetchResultCont: NSFetchedResultsController<Pepole>!
  
  override func viewDidLoad() {
        super.viewDidLoad()

    tableView.delegate = self
    tableView.dataSource = self
    
    tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")

    guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
      return
    }
    
    let managedContext = appDelegate.persistentContainer.viewContext
    let fetchRequest: NSFetchRequest<Pepole> = Pepole.fetchRequest()
    
    fetchRequest.sortDescriptors = [NSSortDescriptor(key: "desgignation", ascending: true), NSSortDescriptor(key: "name", ascending: true)]
    
    self.fetchResultCont = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: managedContext, sectionNameKeyPath: "desgignation", cacheName: nil)
    
  
    do {
      
      try self.fetchResultCont.performFetch()
    } catch {
      fatalError("Failed to fetch entities: \(error)")
    }
    
    self.fetchResultCont.delegate = self
    
    }

  func addPepole(withName: String, withDesi: String) {
    
    guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
      return
    }
    
    let managedContext = appDelegate.persistentContainer.viewContext
    
    let entity = appDelegate.persistentContainer.managedObjectModel.entitiesByName["Pepole"]
    let pepole = Pepole(entity: entity!, insertInto: managedContext)
    pepole.name = withName
    pepole.desgignation = withDesi
    do {
      print(pepole.students)
      
      try managedContext.save()
      print("-----------------------------")
    } catch let error as NSError {
      print("Could not save. \(error), \(error.userInfo)")
    }
    
    
  }

}



extension NSFetchResultViewController: NSFetchedResultsControllerDelegate {
  
  func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {

    self.tableView.reloadData()
  }

  
}

extension NSFetchResultViewController: UITableViewDelegate, UITableViewDataSource {
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return (self.fetchResultCont.sections?.count)!
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.fetchResultCont.sections![section].numberOfObjects
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
    let obj = self.fetchResultCont.object(at: indexPath)
    let name = obj.name ?? ""
    let desi = obj.desgignation ?? ""
    cell.textLabel?.text = "\(name) \(desi)"
    
    return cell
  }
  
  func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    return self.fetchResultCont.sections![section].name
  }
  
  
  func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
    if editingStyle == .delete {
      let obj = self.fetchResultCont.object(at: indexPath)
      //self.tableArray.remove(at: indexPath.row)
      self.fetchResultCont.managedObjectContext.delete(obj)
      do {
        try self.fetchResultCont.managedObjectContext.save()
      } catch {
        print(error)
      }
    }
  }
}
