//
//  HomeViewController.swift
//  abcd
//
//  Created by Rahul Saini on 22/05/18.
//  Copyright © 2018 Rahul Saini. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

  @IBOutlet weak var tableViiew: UITableView!
  override func viewDidLoad() {
        super.viewDidLoad()
    let dict = ["abc":"ffdf","def": nil]
    print(dict)
    print(dict["abc"])
    print(dict["def"])
        self.tableViiew.delegate = self
        self.tableViiew.dataSource = self
    self.tableViiew.rowHeight = 100
        tableViiew.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }
  override func viewWillDisappear(_ animated: Bool) {
    print("a")

  }

}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 3
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
    
    if indexPath.row == 0 {
      cell.textLabel?.text = "Basic Operations With Core Data"
    } else if indexPath.row == 1 {
      cell.textLabel?.text =  "One to many relationship with core data"
    } else if indexPath.row == 2 {
      cell.textLabel?.text =  "NSFetchResultController"
    }
    
    
    return cell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    if indexPath.row == 0 {
      let controller = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController
      self.navigationController?.pushViewController(controller!, animated: true)
      
    } else if indexPath.row == 1 {
      let controller = self.storyboard?.instantiateViewController(withIdentifier: "UsersViewController") as? UsersViewController
      self.navigationController?.pushViewController(controller!, animated: true)
    } else if indexPath.row == 2 {
      let controller = self.storyboard?.instantiateViewController(withIdentifier: "NSFetchResultViewController") as? NSFetchResultViewController
      self.navigationController?.pushViewController(controller!, animated: true)
    }
  }
}
