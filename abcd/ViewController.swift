//
//  ViewController.swift
//  abcd
//
//  Created by Rahul Saini on 20/04/18.
//  Copyright © 2018 Rahul Saini. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {

  
  
  
  
  @IBOutlet weak var tableView: UITableView!
  
  var people: [User] = []
  
  @IBAction func addButtonClicked(_ sender: Any) {
    showAddButtonPopUp()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    self.fetchData()
    
  }
  override func viewDidLoad() {
    super.viewDidLoad()
    tableView.delegate = self
    tableView.dataSource = self
    tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
  }

 

  
  func showUpdatePopUp(indexPath: IndexPath) {
    let alert = UIAlertController(title: "Update User", message: "", preferredStyle: .alert)
    
    let saveAction = UIAlertAction(title: "Save", style: .default) { [unowned self] action in
      
      self.updateData(with: (alert.textFields?.first?.text)!, index: indexPath.row)
      
    }
    
    let dltAction = UIAlertAction(title: "Delete", style: .destructive, handler: { action in
      
      
      self.deleteData(with: self.people[indexPath.row].name!)
      self.people.remove(at: indexPath.row)
      self.tableView.reloadData()
      
    })
    
    let cancelAction = UIAlertAction(title: "Cancel",
                                     style: .default)
    alert.addTextField()
    alert.textFields?.first?.text = people[indexPath.row].name
    alert.textFields?.first?.textAlignment = .center
    
    
    alert.addAction(saveAction)
    alert.addAction(cancelAction)
    alert.addAction(dltAction)
    self.present(alert, animated: true)
  }
  
  
  func showAddButtonPopUp() {
    let alert = UIAlertController(title: "New Name", message: "Add a new name", preferredStyle: .alert)
    
    let saveAction = UIAlertAction(title: "Save", style: .default) { [unowned self] action in
      
      guard let textField = alert.textFields?.first,
        let nameToSave = textField.text else {
          return
      }
      self.save(name: nameToSave, callBack: {
        self.tableView.reloadData()
      })
      
    }
    
    let cancelAction = UIAlertAction(title: "Cancel",
                                     style: .default)
    
    alert.addTextField()
    
    alert.addAction(saveAction)
    alert.addAction(cancelAction)
    self.present(alert, animated: true)
  }
  
  
  
  
  func fetchData() {
    guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
      return
    }
    
    let managedContext = appDelegate.persistentContainer.viewContext
    
    let req: NSFetchRequest<User> = User.fetchRequest()
    do {
      people = try managedContext.fetch(req)
      self.tableView.reloadData()
    } catch let error as NSError {
      print("Could not fetch. \(error), \(error.userInfo)")
    }
  }
  
  
  
  
  func updateData(with name: String, index: Int) {
    
    guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
      return
    }
    
    let managedContext = appDelegate.persistentContainer.viewContext
    
    let req: NSFetchRequest<User> = User.fetchRequest()
    req.predicate = NSPredicate(format: "name = %@", people[index].name!)
  do {
      
      let person = try managedContext.fetch(req)
      if person.count != 0 {
        let obj = person[0]
        obj.name = name
        try managedContext.save()
      }
      self.tableView.reloadData()
    } catch let error as NSError {
      print("Could not fetch. \(error)")
    }
  }
  
  
  
  func deleteData(with name: String) {
    
    guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
      return
    }
    
    let managedContext = appDelegate.persistentContainer.viewContext
    let req: NSFetchRequest<User> = User.fetchRequest()
    req.predicate = NSPredicate(format: "name = %@", name)
    do {
      
      let person = try managedContext.fetch(req)
      
      if person.count != 0 {
        let obj = person[0]
         managedContext.delete(obj)
        try managedContext.save()
      } else {
        let alert = UIAlertController(title: "", message: "Some Error Occured", preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
      }
      
    } catch let error as NSError {
      print("Could not fetch. \(error), \(error.userInfo)")
    }
    
    
  }
  
  
  
  
  
  func save(name: String, callBack: (()->())) {
    
    guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
      return
    }
    
    let managedContext = appDelegate.persistentContainer.viewContext
    
    
    let entity = appDelegate.persistentContainer.managedObjectModel.entitiesByName["User"]
    
    let person = User(entity: entity!, insertInto: managedContext)
    person.name = name
    do {
      try managedContext.save()
      people.append(person)
      callBack()
    } catch let error as NSError {
      callBack()
      managedContext.delete(person)
      print("Could not save. \(error), \(error.userInfo)")
    }
  }
  
  
}


extension ViewController: UITableViewDelegate, UITableViewDataSource{
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return people.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
    cell.textLabel?.text = people[indexPath.row].name
    return cell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    self.showUpdatePopUp(indexPath: indexPath)
  }
  
}
