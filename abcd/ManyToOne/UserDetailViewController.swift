//
//  UserDetailViewController.swift
//  abcd
//
//  Created by Rahul Saini on 24/05/18.
//  Copyright © 2018 Rahul Saini. All rights reserved.
//

import UIKit

class UserDetailViewController: UIViewController {

  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var lastNameLabel: UILabel!
  @IBOutlet weak var firstNameLabel: UILabel!
  
  var user = UserWithContact() {
    didSet {
      //self.tableView.reloadData()
    }
  }
  
  override func viewDidLoad() {
        super.viewDidLoad()
    tableView.delegate = self
    tableView.dataSource = self
    self.lastNameLabel.text = self.user.lastName
    self.firstNameLabel.text =  self.user.firtName
    tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }


}


extension UserDetailViewController: UITableViewDelegate, UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.user.contacts?.count ?? 0
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
    let contacts = self.user.contacts?.allObjects as? [Contact]
    cell.textLabel?.text = contacts![indexPath.row].mobie
    return cell
  }
}
