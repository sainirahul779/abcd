//
//  AddUserPopUp.swift
//  abcd
//
//  Created by Rahul Saini on 22/05/18.
//  Copyright © 2018 Rahul Saini. All rights reserved.
//

import UIKit

class AddUserPopUp: UIViewController {

  var numberOfContacts = 1
  
  var callBack: ((_ user: UserWithContact)->())? = nil
  @IBOutlet weak var tableView: UITableView!
  
  @IBAction func confirmButtonClicked(_ sender: Any) {
    
    guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
      return
    }
    
    let managedContext = appDelegate.persistentContainer.viewContext
    
    let entity = appDelegate.persistentContainer.managedObjectModel.entitiesByName["UserWithContact"]
    let userData = UserWithContact(entity: entity!, insertInto: managedContext)
    
    var cell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? TextFieldTableViewCell
    userData.firtName = cell?.field.text
    
    cell = tableView.cellForRow(at: IndexPath(row: 1, section: 0)) as? TextFieldTableViewCell
    userData.lastName = cell?.field.text
    
    for i in 0..<numberOfContacts {
      cell = tableView.cellForRow(at: IndexPath(row: i, section: 1)) as? TextFieldTableViewCell
      let contactEntity = appDelegate.persistentContainer.managedObjectModel.entitiesByName["Contact"]
      let contact = Contact(entity: contactEntity!, insertInto: managedContext)
      contact.mobie = cell?.field.text
      userData.addToContacts(contact)
    }
    
    
    do {
      try managedContext.save()
      if let call = callBack {
        call(userData)
        self.dismiss(animated: true, completion: nil)
      }
      //userData.append(person)
      
    } catch let error as NSError {
      print("Could not save. \(error), \(error.userInfo)")
    }
  }
  
  override func viewDidLoad() {
        super.viewDidLoad()
      tableView.delegate = self
      tableView.dataSource = self
      self.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
      tableView.register(UINib(nibName: "TextFieldTableViewCell", bundle: nil), forCellReuseIdentifier: "TextFieldTableViewCell")
     // tableView.registerHeaderFooter("AddUserHeaderView", identifier: "AddUserHeaderView", bundle: nil)
      tableView.register(UINib(nibName: "AddUserHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: "AddUserHeaderView")
      
    }


}


extension AddUserPopUp: UITableViewDataSource, UITableViewDelegate {
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 2
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if section == 0 {
      return 2
    }
      return numberOfContacts
    
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    guard let cell = tableView.dequeueReusableCell(withIdentifier: "TextFieldTableViewCell") as? TextFieldTableViewCell else {
    fatalError("Error")
    }
    
    if indexPath.section == 0 {
      
      if indexPath.row == 0 {
        
          cell.label.text = "First Name:"
          cell.field.placeholder = "Enter First Name"
        
      } else if indexPath.row == 1 {
        cell.label.text = "Last Name:"
        cell.field.placeholder = "Last First Name"
        
      }
      
    } else if indexPath.section == 1 {
      cell.label.text = "Contact Number \(indexPath.row + 1):"
      cell.field.placeholder = "Enter Contacts"
    }
    return cell
  }
  
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    guard let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "AddUserHeaderView") as? AddUserHeaderView else {
      fatalError("AddUserHeaderView")
    }
    if section == 0 {
      header.titleLabel.text = "USER INFO:"
      header.addbutton.isHidden = true
      
    } else if section == 1 {
      header.titleLabel.text = "USER Contacts:"
      header.addbutton.setTitle("Add Contact", for: .normal)
      header.addbutton.isHidden = false
      header.callBack = {
        self.numberOfContacts += 1
        self.tableView.reloadData()
      }
    }
    return header
  }
  
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return 80.00
  }
}
