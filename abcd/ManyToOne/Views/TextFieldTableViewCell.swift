//
//  TextFieldTableViewCell.swift
//  abcd
//
//  Created by Rahul Saini on 22/05/18.
//  Copyright © 2018 Rahul Saini. All rights reserved.
//

import UIKit

class TextFieldTableViewCell: UITableViewCell {

  
  @IBOutlet weak var label: UILabel!
  @IBOutlet weak var field: UITextField!
  
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
