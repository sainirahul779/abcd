//
//  AddUserHeaderView.swift
//  abcd
//
//  Created by Rahul Saini on 22/05/18.
//  Copyright © 2018 Rahul Saini. All rights reserved.
//

import UIKit

class AddUserHeaderView: UITableViewHeaderFooterView {

  @IBOutlet weak var addbutton: UIButton!
  @IBOutlet weak var titleLabel: UILabel!
  
  var callBack: (()->())? = nil
  @IBAction func buttonClicked(_ sender: Any) {
    if let call = callBack {
      call()
    }
  }
}
