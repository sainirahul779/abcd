//
//  UsersViewController.swift
//  abcd
//
//  Created by Rahul Saini on 22/05/18.
//  Copyright © 2018 Rahul Saini. All rights reserved.
//

import UIKit
import CoreData

class UsersViewController: UIViewController {

  
  @IBAction func adduserButtonClicked(_ sender: Any) {
    
    print("__________________")
    self.openAddUserCont()
  }
  
  
  
  @IBOutlet weak var tableView: UITableView!
  var users: [UserWithContact] = [UserWithContact]()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    print("b")
    tableView.delegate = self
    tableView.dataSource = self
    tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    self.fetchUser()
    
    }

  override func viewWillAppear(_ animated: Bool) {
    print("c")

  }

  func openAddUserCont() {
    
    let addUserPopUp = self.storyboard?.instantiateViewController(withIdentifier: "AddUserPopUp") as? AddUserPopUp
   
    addUserPopUp?.callBack = {(user: UserWithContact) in
      
      self.users.append(user)
      self.tableView.reloadData()
    }
    addUserPopUp?.modalPresentationStyle = .overCurrentContext
    addUserPopUp?.modalTransitionStyle = .crossDissolve
    self.present(addUserPopUp!, animated: true, completion: nil)
  }
  
  
  
  
  func fetchUser() {
    guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
      return
    }
    
    let managedContext = appDelegate.persistentContainer.viewContext
    
    let request: NSFetchRequest<UserWithContact> = UserWithContact.fetchRequest()
 
    do {
        let users = try managedContext.fetch(request)
  
      self.users = users
      self.tableView.reloadData()
    } catch let error as NSError {
      print("Could not save. \(error), \(error.userInfo)")
    }
  }
  
  
  
}


extension UsersViewController: UITableViewDataSource, UITableViewDelegate {
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
     return self.users.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
    cell.textLabel?.text = "\(self.users[indexPath.row].firtName ?? "") \(self.users[indexPath.row].lastName ?? "")"
    return cell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let userForShow = self.users[indexPath.row]
    
    let cont = self.storyboard?.instantiateViewController(withIdentifier: "UserDetailViewController") as? UserDetailViewController
    
    cont?.user = userForShow
    self.navigationController?.pushViewController(cont!, animated: true)
  }
}
