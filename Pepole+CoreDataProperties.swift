//
//  Pepole+CoreDataProperties.swift
//  abcd
//
//  Created by Rahul Saini on 29/05/18.
//  Copyright © 2018 Rahul Saini. All rights reserved.
//
//

import Foundation
import CoreData


extension Pepole {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Pepole> {
        return NSFetchRequest<Pepole>(entityName: "Pepole")
    }

    @NSManaged public var desgignation: String?
    @NSManaged public var name: String?

}
