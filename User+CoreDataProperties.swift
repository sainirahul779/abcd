//
//  User+CoreDataProperties.swift
//  abcd
//
//  Created by Rahul Saini on 29/05/18.
//  Copyright © 2018 Rahul Saini. All rights reserved.
//
//

import Foundation
import CoreData


extension User {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<User> {
        return NSFetchRequest<User>(entityName: "User")
    }

    @NSManaged public var name: String?

}
