//
//  Pepole+CoreDataClass.swift
//  abcd
//
//  Created by Rahul Saini on 29/05/18.
//  Copyright © 2018 Rahul Saini. All rights reserved.
//
//

import Foundation
import CoreData

@objc(Pepole)
public class Pepole: NSManagedObject {
  
  @NSManaged var students: [Pepole]

}
