//
//  Contact+CoreDataProperties.swift
//  abcd
//
//  Created by Rahul Saini on 29/05/18.
//  Copyright © 2018 Rahul Saini. All rights reserved.
//
//

import Foundation
import CoreData


extension Contact {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Contact> {
        return NSFetchRequest<Contact>(entityName: "Contact")
    }

    @NSManaged public var mobie: String?
    @NSManaged public var user: UserWithContact?

}
